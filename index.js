var express = require("express");
var axios = require("axios");

var app = express();

// Test d'un message JSON: je ne sais pas si c'est comme ça que l'on fait.
app.get("/", function(req, res) {
  res.json({ success: true, message: "Mon maitre de stage est fou" });
});

// Pour insérer un message, je faisais comme ça
app.get("/message", function(req, res) {
  res.setHeader("Content-Type", "text/plain");
  res.send("Voici comment j'insère un message");
});

app.get("/posts", async (req, res) => {
  try {
    const responses = await axios.get(
      "https://jsonplaceholder.typicode.com/posts"
    );

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    // La méthode filter va te renvoyer un nouvelle array avec les items qui match la condition
    // la condition ici est la présence du mot consequatur dans le body de l'item
    const posts = responses.data.filter(
      resp => resp.body.indexOf("consequatur") != -1
    );

    return res.status(200).json({ success: true, posts: posts });
  } catch (error) {
    console.log(error);

    return res
      .status(500)
      .json({ success: false, error: "Une erreur est survenue." });
  }
});

// Pour gérer une page introuvable
app.use(function(req, res, next) {
  res.setHeader("Content-Type", "text/plain");
  res.status(404).send("Page introuvable !");
});

app.listen(8080, () => {
  console.log("Server is listening on port 8080");
});
